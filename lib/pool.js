'use strict';

const Abstract = require('@scola/transport-pool');
const Error = require('@scola/error');

class PublisherPool extends Abstract.Pool {
  send(message) {
    super.send(message);

    this.connections.forEach((connection) => {
      if (connection.canSend()) {
        return connection.send(message);
      }

      return this.emit('error', new Error('transport_message_not_sent', {
        detail: {
          message
        }
      }));
    });
  }
}

module.exports = PublisherPool;
